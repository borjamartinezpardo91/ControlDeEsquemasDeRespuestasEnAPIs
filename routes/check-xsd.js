var express = require('express');
var router = express.Router();
const newCheck = require('../src/checkXsd').newCheck;

/* GET home page. */
router.get('/', function(req, res, next) {
	const folderName = req.query.folderName;
	const firstFile = req.query.firstFile;
	const secondFile = req.query.secondFile;
	if (folderName && firstFile && secondFile) {
		newCheck(folderName, firstFile, secondFile)
		.then(diffs => {
			res.status(200).json({
				diffs: diffs
			});
		})
		.catch(error => {
			res.status(400).json({
				error: error
			});
		});
	}
});

module.exports = router;