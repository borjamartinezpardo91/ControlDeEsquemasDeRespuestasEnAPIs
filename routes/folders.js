var express = require('express');
var router = express.Router();
var fs = require('fs');

/* GET home page. */
router.get('/:name', function(req, res, next) {
    try {
        var path = 'src/xsd/' + req.params.name;
        var xsdFiles = [];
        fs.readdirSync(path).forEach(xsdFile => {
            xsdFiles.push(xsdFile);
        });
        res.status(200).json({
            files: xsdFiles
        });
    } catch (error) {
        res.status(500)
        .json({
            files: null
        });
    }
});

router.get('/', function(req, res, next) {
    try {
        var path = 'src/xsd/';
        var folders = [];
        fs.readdirSync(path).forEach(folder => {
            var numberOfFiles = 0;
            fs.readdirSync(path + folder + '/').forEach(xsdFile => {
                if (xsdFile) {
                    numberOfFiles++;
                }
            });
            var xsdFolder = {
                name: folder,
                numberOfFiles: numberOfFiles
            }
            folders.push(xsdFolder);
        });
        res.status(200).json({
            folders
        });
    } catch (error) {
        res.status(500).json({error});
    }
});

module.exports = router;