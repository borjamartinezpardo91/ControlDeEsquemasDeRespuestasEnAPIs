var express = require('express');
var multer = require("multer");
var router = express.Router();
var mkdirp =  require('mkdirp');
var fs = require('fs');
var formidable = require('formidable');

var storage = multer.diskStorage({
	//multers disk storage settings
	destination: function (req, file, cb) {
		var fileName = file.originalname.split('.')[0];
		cb(null, 'src/xsd/' + fileName);
	},
	filename: function (req, file, cb) {
		if (file) {
			var fileName = file.originalname.split('.')[0];
			var fileExtension = file.originalname.split('.')[1];
			var path = 'src/xsd/' + fileName + '/';
			//Checks if the folder already exist
			let maxIndex = 0;
			if (!fs.existsSync(path)) {
				mkdirp.sync(path, function (err) {
					if (err) {
						console.error(err);
					}
				});
			} else {
				fs.readdirSync(path).forEach(file => {
					var fileIndex = file.split('-')[1].split('.')[0];
					if (fileIndex > maxIndex) {
						maxIndex = fileIndex;
					}
				});
			}
			cb(null, fileName + '-' + ++maxIndex + '.' + fileExtension);
		} else {
			res.write('No file to upload!');
			res.end();
		}
	}
});

var upload = multer({
	//multer settings
	storage: storage
}).single("file");

router.post('/', function (req, res, next) {
	var path = '';
	upload(req, res, function (err) {
	   if (err) {
		 // An error occurred when uploading
		 console.log(err);
		 return res.status(422).send('an Error occured');
	   }  
	  // No error occured.
	   path = req.file.path;
	   return res.send('Upload Completed for ' + path); 
 	});     
});

module.exports = router;