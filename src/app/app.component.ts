import { Component, Inject, OnInit, OnDestroy } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { FileUploader } from "ng2-file-upload";
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { HttpParams } from '@angular/common/http';
import { Observable } from "rxjs";
import { TimerObservable } from "rxjs/observable/TimerObservable";
import 'rxjs/add/operator/takeWhile';

const URL = "uploadfile";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit, OnDestroy {
	public diffs: Object;
	public isDataLoaded: boolean = false;
	public uploadNewSchemas: boolean = false;
	public uploader: FileUploader = new FileUploader({ url: URL });
	public folders: any[];
	private file;
	private alive = false;

	constructor(private http: HttpClient,
		public snackBar: MatSnackBar,
		public dialog: MatDialog) {
			this.alive = true;
	}

	ngOnInit() {
		this.uploadNewSchemas = true;
		TimerObservable.create(0, 4000)
		.takeWhile(() => this.alive)
		.subscribe(() => {
			this.http.get('/folders').subscribe(
				data => {
					this.folders = data['folders'];
				}, error => {
					this.snackBar.open('An unexpected error occured', 'Close', {
						duration: 1500
					});
				}
			);
		});
	}

	ngOnDestroy() {
		this.alive = false;
	}

	CheckXsd() {
		this.http.get("/checkxsd").subscribe(
			data => {
				this.diffs = data["diffs"];
				this.isDataLoaded = true;
			},
			error => {
				this.snackBar.open('An unexpected error occured', 'Close', {
					duration: 1500
				});
			}
		);
	}

	compareFiles(folderName: string, firstPath: string, secondPath: string) {
		const options = {
			params: new HttpParams()
						.set('folderName', folderName)
						.set('firstFile', firstPath)
						.set('secondFile', secondPath)
		}
		this.http.get('/checkxsd', options).subscribe(
			data => {
				this.diffs = data["diffs"];
				this.isDataLoaded = true;
			},
			error => {
				this.snackBar.open('An unexpected error occured', 'Close', {
					duration: 1500
				});
			}
		);
	}
	
	uploadSchemas = () => {
		this.uploadNewSchemas = true;
	}

	showFiles = (folderName: string) => {
		this.uploadNewSchemas = false;
		this.isDataLoaded = false;
		
		this.http.get('/folders/' + folderName)
		.subscribe(reqData => {
			var files = reqData['files'];
			var checkedValues = [];
			if (files) {
				let dialogRef = this.dialog.open(DialogFiles, {
					width: '250px',
					data: { 
						folderName: folderName, 
						files: files,
						checkedValues: checkedValues,
						validForm() {
							let numberOfChecksSelected = 0;
							for (let i = 0; i < checkedValues.length; i++) {
								if (checkedValues[i] === true) {
									numberOfChecksSelected++;
								}
							}
							return numberOfChecksSelected === 2;
						}
					}
				});
				dialogRef.afterClosed().subscribe(result => {
					if (result === true) {
						let firstPath: string = null;
						let secondPath: string = null;
						for (let i = 0; i < checkedValues.length; i++) {
							if (checkedValues[i] === true) {
								if (!firstPath) {
									firstPath = files[i];
								} else {
									secondPath = files[i];
								}
							}
						}
						if (folderName && firstPath && secondPath) {
							this.compareFiles(folderName, firstPath, secondPath);
						}
					}
				});				
			} else {
				throw new Error();
			}
		},
		error => {
			this.snackBar.open('An unexpected error occured', 'Close', {
				duration: 1500
			});
		});

	}

	updateFile = (element) => {
		console.log('changed')
		this.file = element.files;
	}

	onSubmit = () => {
		console.log('submit');
		var fd = new FormData();
        fd.append('files', this.file);
		this.http.post('/uploadfile', fd)
			.subscribe(data => {
				console.log('ok');
			}, error => {
				console.log(error);
			});
	}
}

@Component({
	selector: 'dialog-files',
	templateUrl: 'dialog-files.html',
})
export class DialogFiles {

	constructor(
		public dialogRef: MatDialogRef<DialogFiles>,
		@Inject(MAT_DIALOG_DATA) public data: any) { }
	
	onNoClick(): void {
		this.dialogRef.close();
	}

}
