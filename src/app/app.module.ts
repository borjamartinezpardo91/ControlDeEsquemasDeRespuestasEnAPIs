import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppComponent } from "./app.component";
import { MatTabsModule, MatListModule, MatButtonModule, MatCardModule, MatToolbarModule, MatProgressSpinnerModule } from "@angular/material";
import { FileUploadModule } from "ng2-file-upload";
import { FileUploader } from "ng2-file-upload";
import { MatDialogModule } from '@angular/material';
import { MatSnackBarModule } from '@angular/material';
import { DialogFiles } from './app.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';

@NgModule({
	declarations: [
		AppComponent,
		DialogFiles
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		MatTabsModule,
		MatListModule,
		HttpClientModule,
		FileUploadModule,
		MatButtonModule,
		MatCardModule,
		MatToolbarModule,
		MatProgressSpinnerModule,
		MatSnackBarModule,
		MatDialogModule,
		MatCheckboxModule,
		FormsModule
	],
	providers: [],
	bootstrap: [AppComponent],
	entryComponents: [DialogFiles]
})
export class AppModule {}
