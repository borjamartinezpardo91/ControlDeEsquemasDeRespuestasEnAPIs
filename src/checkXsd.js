const fs = require("fs");
const parser = require("xml2js").Parser();
const _ = require("lodash");
const X2JS = require("x2js");

const XSD_DIR = 'src/xsd/';
const DIFFS_PATH = __dirname + "/xsd/diffs.json";

function checkXsd() {
	return new Promise((fulfill, reject) => {
		var lastFileNumber = getLastFileNumber();
		if (lastFileNumber === 0) {
			createXsdCopy(1)
				.then(() => checkDiffs(1))
				.then(() => returnDiffs())
				.then(diffs => fulfill(diffs))
				.catch(error => reject(error));
		} else {
			checkDiffs(lastFileNumber)
				.then(diffs => writeDiffs(diffs))
				.then(() => createXsdCopy(lastFileNumber + 1))
				.then(() => returnDiffs())
				.then(diffs => fulfill(diffs))
				.catch(error => reject(error));
		}
	});
}

function getLastFileNumber() {
	var files = fs.readdirSync(XSD_DIR);
	var maxValue = 0;
	const regexp = /^users([0-9]*).xsd$/;
	for (let i = 0; i < files.length; i++) {
		var match = regexp.exec(files[i]);
		if (match && parseInt(match[1]) > maxValue) {
			maxValue = match[1];
		}
	}
	return parseInt(maxValue);
}

function createXsdCopy(number) {
	return new Promise((fulfill, reject) => {
		fs.readFile(XSD_DIR + "users.xsd", (err, data) => {
			if (err) return reject(err);
			fs.writeFile(XSD_DIR + "users" + number + ".xsd", data, err => {
				return fulfill();
			});
		});
	});
}

function newCheck(folderName, firstFile, secondFile) {
	return new Promise((fulfill, reject) => {
		checkDiffs(folderName, firstFile, secondFile)
			.then(diffs => {
				fulfill(diffs)
			})
			.catch(error => {
				reject(error)
			});
	});
}

function checkDiffs(folderName, firstFile, secondFile) {
	return new Promise((fulfill, reject) => {
		var origFile = fs.readFileSync(XSD_DIR + folderName + '/' + firstFile).toString();
		var copyFile = fs.readFileSync(XSD_DIR + folderName + '/' + secondFile).toString();
		Promise.all([getJSONFromXml(origFile), getJSONFromXml(copyFile)])
			.then(jsons => {
				var origJSON = jsons[0];
				var copyJSON = jsons[1];
				var diffs = {
					adds: [],
					changes: [],
					rems: []
				};
				const changes = compare(origJSON, copyJSON);
				if (changes.different.length > 0) {
					for (let i = 0; i < changes.different.length; i++) {
						const difference = changes.different[i];
						const jsonPath = difference.split(".");
						const originalChange = _.at(origJSON, difference);
						const modifiedChange = _.at(copyJSON, difference);
						var change = { previousValue: modifiedChange, currentValue: originalChange };
						diffs["changes"].push(logChange(jsonPath, originalChange, modifiedChange));
					}
				}
				if (changes.missing_from_first.length > 0) {
					for (let i = 0; i < changes.missing_from_first.length; i++) {
						const newChange = changes.missing_from_first[i];
						const jsonPath = newChange.split(".");
						const originalChange = _.at(origJSON, newChange);
						const modifiedChange = _.at(copyJSON, newChange);
						var rem = { previousValue: modifiedChange, currentValue: originalChange };
						diffs["adds"].push(logChange(jsonPath, originalChange, modifiedChange));
					}
				}
				if (changes.missing_from_second.length > 0) {
					for (let i = 0; i < changes.missing_from_second.length; i++) {
						const newChange = changes.missing_from_second[i];
						const jsonPath = newChange.split(".");
						const originalChange = _.at(origJSON, newChange);
						const modifiedChange = _.at(copyJSON, newChange);
						var add = { previousValue: modifiedChange, currentValue: originalChange };
						diffs["rems"].push(logChange(jsonPath, originalChange, modifiedChange));
					}
				}
				return fulfill(diffs);
			})
			.catch(error => reject(error));
	});
}

const logChange = (jsonPath, originalChange, modifiedChange) => {
	const lastBranch = jsonPath[jsonPath.length - 1];
	if (_.startsWith(lastBranch, "_")) {
		const attrName = lastBranch.substr(1);
		return "Attribute " + attrName + " change from " + originalChange + " to " + modifiedChange;
	} else if (_.isFinite(_.toNumber(lastBranch))) {
		const x2 = new X2JS();
		const originalText = x2.js2xml(originalChange).replace(/:0/g, ":" + jsonPath[jsonPath.length - 2]);
		const modifiedText = x2.js2xml(modifiedChange).replace(/:0/g, ":" + jsonPath[jsonPath.length - 2]);
		if (originalChange && !originalChange[0] && modifiedChange && modifiedChange[0]) {
			return "Element added: " + modifiedText;
		} else if (originalChange && originalChange[0] && modifiedChange && !modifiedChange[0]) {
			return "Element removed: " + originalText;
		}
	} else {
		const x2 = new X2JS();
		const originalText = x2.js2xml(originalChange).replace(/:0/g, ":" + jsonPath[jsonPath.length - 1]);
		const modifiedText = x2.js2xml(modifiedChange).replace(/:0/g, ":" + jsonPath[jsonPath.length - 1]);
		if (originalChange && !originalChange[0] && modifiedChange && modifiedChange[0]) {
			return "Element added: " + modifiedText;
		} else if (originalChange && originalChange[0] && modifiedChange && !modifiedChange[0]) {
			return "Element removed: " + originalText;
		}
	}
};

function getJSONFromXml(xml) {
	const x2js = new X2JS({arrayAccessFormPaths: []});
	return x2js.xml2js(xml);
}

function getDifferences(origElements, copyElements) {
	var diffs = {
		adds: [],
		changes: [],
		rems: []
	};
	if (origElements.length > copyElements.length) {
		for (let i = copyElements.length; i < origElements.length; i++) {
			diffs["adds"].push({
				name: origElements[i]["$"]["name"],
				type: origElements[i]["$"]["type"]
			});
		}
	} else if (origElements.length < copyElements.length) {
		for (let i = origElements.length; i < copyElements.length; i++) {
			diffs["rems"].push({
				name: copyElements[i]["$"]["name"],
				type: copyElements[i]["$"]["type"]
			});
		}
	}
	for (let i = 0; i < origElements.length; i++) {
		if (i < copyElements.length) {
			if (
				origElements[i]["$"]["name"] !== copyElements[i]["$"]["name"] ||
				origElements[i]["$"]["type"] !== copyElements[i]["$"]["type"]
			) {
				diffs["changes"].push({
					element: i + 1,
					previousValue: copyElements[i]["$"],
					currentValue: origElements[i]["$"]
				});
			}
		}
	}
	return diffs;
}

function writeDiffs(diffs) {
	return new Promise((fulfill, reject) => {
		fs.writeFile(DIFFS_PATH, JSON.stringify(diffs), err => {
			if (err) return reject(err);
			return fulfill();
		});
	});
}

function returnDiffs() {
	return new Promise((fulfill, reject) => {
		fs.readFile(DIFFS_PATH, (err, data) => {
			if (err) return reject(err);
			return fulfill(JSON.parse(data.toString()));
		});
	});
}

const compare = function (a, b) {
	var result = {
	  different: [],
	  missing_from_first: [],
	  missing_from_second: []
	};
  
	_.reduce(a, function (result, value, key) {
	  if (b.hasOwnProperty(key)) {
		if (_.isEqual(value, b[key])) {
		  return result;
		} else {
		  if (typeof (a[key]) != typeof ({}) || typeof (b[key]) != typeof ({})) {
			//dead end.
			result.different.push(key);
			return result;
		  } else {
			let deeper;
			if (a[key] instanceof Array && !(b[key] instanceof Array)) {
			  deeper = {
				different: [],
				missing_from_first: [],
				missing_from_second: []
			  };
			  for (let i = 0; i < a[key].length; i++) {
				if (!_.isEqual(a[key][i], b[key])) {
				  deeper.missing_from_second.push(i);
				}
			  }
			} else if (!(a[key] instanceof Array) && b[key] instanceof Array)  {
			  deeper = {
				different: [],
				missing_from_first: [],
				missing_from_second: []
			  };
			  for (let i = 0; i < b[key].length; i++) {
				if (!_.isEqual(b[key][i], a[key])) {
				  deeper.missing_from_first.push(i);
				}
			  }
			} else {
			  deeper = compare(a[key], b[key]);
			}
			result.different = result.different.concat(_.map(deeper.different, (sub_path) => {
			  return key + "." + sub_path;
			}));
  
			result.missing_from_second = result.missing_from_second.concat(_.map(deeper.missing_from_second, (sub_path) => {
			  return key + "." + sub_path;
			}));
  
			result.missing_from_first = result.missing_from_first.concat(_.map(deeper.missing_from_first, (sub_path) => {
			  return key + "." + sub_path;
			}));
			return result;
		  }
		}
	  } else {
		result.missing_from_second.push(key);
		return result;
	  }
	}, result);
  
	_.reduce(b, function (result, value, key) {
	  if (a.hasOwnProperty(key)) {
		return result;
	  } else {
		result.missing_from_first.push(key);
		return result;
	  }
	}, result);
  
	return result;
  }

exports.checkXsd = checkXsd;
exports.newCheck = newCheck;